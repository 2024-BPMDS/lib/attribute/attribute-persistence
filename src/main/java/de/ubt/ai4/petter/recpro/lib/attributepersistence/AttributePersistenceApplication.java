package de.ubt.ai4.petter.recpro.lib.attributepersistence;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AttributePersistenceApplication {

	public static void main(String[] args) {
		SpringApplication.run(AttributePersistenceApplication.class, args);
	}

}
