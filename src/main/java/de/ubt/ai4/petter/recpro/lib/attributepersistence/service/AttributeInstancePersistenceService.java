package de.ubt.ai4.petter.recpro.lib.attributepersistence.service;

import de.ubt.ai4.petter.recpro.lib.attributepersistence.model.*;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class AttributeInstancePersistenceService {

    private AttributeInstanceRepository repository;

    public RecproAttributeInstance saveAndFlush(RecproAttributeInstance instance) {
        switch (instance.getAttributeType()) {
            case TEXT -> {
                return repository.saveAndFlush((RecproTextualAttributeInstance) instance);
            }
            case BINARY -> {
                return repository.saveAndFlush((RecproBinaryAttributeInstance) instance);
            }
            case NUMERIC -> {
                return repository.saveAndFlush((RecproNumericAttributeInstance) instance);
            }
            default -> {
                return repository.saveAndFlush((RecproMetaAttributeInstance) instance);
            }
        }
    }

    public List<RecproAttributeInstance> getAll() {
        return this.repository.findAll();
    }

    public RecproAttributeInstance getByRecproElementInstanceId(RecproAttributeInstance attributeInstance) {
        List<RecproAttributeInstance> test = this.repository.findByRecproElementInstanceIdAndRecproAttributeIdAndRecproProcessInstanceIdOrderByIdDesc(attributeInstance.getRecproElementInstanceId(), attributeInstance.getRecproAttributeId(), attributeInstance.getRecproProcessInstanceId());
        if (test.isEmpty()) {
            test = this.repository.findByRecproAttributeIdAndRecproProcessInstanceIdOrderByIdDesc(attributeInstance.getRecproAttributeId(), attributeInstance.getRecproProcessInstanceId());
            if (test.isEmpty()) {
                return this.saveAndFlush(attributeInstance);
            } else {
                RecproAttributeInstance res = test.get(0).copy();
                res.setRecproElementInstanceId(attributeInstance.getRecproElementInstanceId());
                return res;
            }
        } else {
            return test.get(0);
        }
    }

    public List<RecproAttributeInstance> getByProcessInstanceIds(List<String> processInstanceIds) {
        return repository.findAllByRecproProcessInstanceIdIn(processInstanceIds);
    }
}
